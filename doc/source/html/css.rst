.. _`ch_css`:

Cascading Style Sheets (CSS)
****************************


Introduction
============

CSS is used to enhance the look of the web page. In :numref:`sec_core_attr`, we saw the attribute 'style', which is used for changing the color of the text. Let's rewrite the example of 'style' as shown in next section. 

Inline CSS
----------
    
* Below code is an example of 'inline CSS', where the styles are defined inside the individual tags. 
    

.. code-block:: html
    :linenos:

    <!-- css.html -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>CSS Tutorial</title>

    </head>
    <body>

        <h3 style="color:blue"> Heading 1 </h3>
        <h3 style="color:blue"> Heading 3 </h3>
        <h3 style="color:blue"> Heading 3 </h3>

    </body>
    </html>

In the above code, we have three 'headings' with font-color as 'blue'. Suppose, we want to change the color to red, then we must go to to individual 'h3' tag and then change the color. This is easy in this case, but if we have 100 headings in 5 different 'html' files, then this process is not very handy. In such cases, CSS can be quite useful as shown in next section.  

Embedded CSS
------------

In the below code, the style is embedded inside the 'style' tag as shown in Lines 8-17. Here, we have defined two classes i.e. 'h3_blue (Lines 21-23)' and 'h3_red (Lines 26-28)'. Then, the **selectors** at Lines 9 and 13 targets the class 'h3_blue' \& 'h3_red', and change the color to blue and red respectively. In this chapter, we will discuss the selectors (e.g. h3.h3_blue) in more details. 

.. note:: 
    
    * In CSS, the comments are written between /\* and \*/. 
    * CSS has three parts, 
        
        * **Selectors**  e.g. p, h3.h3_blue
        * **Properties**  e.g. color
        * **Values** of properties e.g. red


.. code-block:: html
    :linenos:
    :emphasize-lines: 8-17, 21, 26

    <!-- css.html -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>CSS Tutorial</title>

        <style type="text/css">
            h3.h3_blue{     /*change color to blue*/
                color: blue;
            }

            h3.h3_red{      /*change color to red*/
                color:red;
            }
        </style>
        
    </head>
    <body>

        <h3 class='h3_blue'> Heading 1 </h3>
        <h3 class='h3_blue'> Heading 3 </h3>
        <h3 class='h3_blue'> Heading 3 </h3>


        <h3 class='h3_red'> Heading 1 </h3>
        <h3 class='h3_red'> Heading 3 </h3>
        <h3 class='h3_red'> Heading 3 </h3>

    </body>
    </html>


* Below is the output of above code, 

.. _`fig_css1`:

.. figure:: fig/css1.png

   Embedded CSS 



External CSS
------------

We can write the 'CSS' code in different file and then import the file into 'html' document as shown in this section. In this way, we can manage the files easily. 

* The 'CSS' code is saved in the file 'my_css.css' which is saved inside the folder 'asset/css'. 

.. code-block:: css

    /* asset/css/my_css.css */

    h3.h3_blue{
        color: blue;
    }

    h3.h3_red{
        color:red;
    }

* Next, we need to import the CSS file into the 'html' file as shown in Line 7. 

.. code-block:: html
    :linenos:
    :emphasize-lines: 7

    <!-- css.html -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>CSS Tutorial</title>
        <link rel="stylesheet" type="text/css" href="asset/css/my_css.css">
    </head>
    <body>

        <h3 class='h3_blue'> Heading 1 </h3>
        <h3 class='h3_blue'> Heading 3 </h3>
        <h3 class='h3_blue'> Heading 3 </h3>


        <h3 class='h3_red'> Heading 1 </h3>
        <h3 class='h3_red'> Heading 3 </h3>
        <h3 class='h3_red'> Heading 3 </h3>

    </body>
    </html>


Basic CSS Selectors
===================


There are three types of selectors in CSS, 

* **Element** : can be selected using it's name e.g. 'p', 'div' and 'h1' etc.  
* **Class** : can be selected using '.className' operator e.g. '.h3_blue'. 
* **ID** : can be selected using '#idName' e.g. '#my_para'. 

We will use following HTML for understanding the selectors, 

.. code-block:: html
    :linenos:

    <!-- css.html -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>CSS Selectors</title>
        <link rel="stylesheet" type="text/css" href="asset/css/my_css.css">
    </head>
    <body>
        <h3>CSS Selectors</h3>

        <p class='c_head'> Paragraph with class 'c_head' </p>
        <p id='i_head'> Paragraph with id 'i_head' </p>

    </body>
    </html>


* Below code shows the example of different selectors, and the output is shown in :numref:`fig_css2`
  
.. code-block:: css
    :linenos:

    /* asset/css/my_css.css */

    /*element selection*/
    h3 {
        color: blue;
    }


    /*class selection*/
    .c_head{
        font-family: cursive;
        color: orange;
    }


    /*id selection*/
    #i_head{
        font-variant: small-caps;
        color: red;
    }

.. _`fig_css2`:

.. figure:: fig/css2.png

   Selectors : element, class and id



Hierarchy
=========

In previous section, we saw the example of selectors. In this section, we will understand the hierarchy of the styling-operations. 

.. important:: 

    Below is the priority level for the CSS, 

    * Priority level :

        - ID (highest priority)
        - Class
        - Element


    * If two CSS has same priority, then CSS rule at the last will be applicable. 



* Below is the html code with following tags, 

    - 'p' tag 
    - 'p' tag with class 'c_head'
    - 'p' tag with class 'c_head' and id 'i_head'

.. code-block:: html
    :linenos:

    <!-- css.html -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>CSS Selectors</title>
        <link rel="stylesheet" type="text/css" href="asset/css/my_css.css">
    </head>
    <body>
        <p>Paragraph</p>

        <p class='c_head'> Paragraph with class 'c_head' </p>
        <p class='c_head' id='i_head'> Paragraph with class 'c_head' and id 'i_head' </p>

    </body>
    </html>



Below is the CSS code. Let's understand the formatting of all three 'p' tags individually. The results are shown in :numref:`fig_css3`. 

* **'p' tag at Line 13 of html** : Since, 'id' has highest priority, therefore CSS rule for #i_head' (Line 12) will not be overridden by Line 24; hence the color is **red**. Line 13 has 'p' tag, therefore 'font-variant' rule will be applied by Line 17. Also, this tag has class 'c_head', therefore 'font' will be set to 'cursive'. Hence, the line is "**all-caps with font-cursive in red color**". 

* **'p' tag at Line 12 of html** : Similarly, the 'head' tag has higher priority than 'element' therefore color of this line is **oranage** and font-family is 'cursive'. Also, Line 17 will make it **all caps**

* **'p' tag at Line 10 of html** : Color defined at Line 18 will be overridden by Line 24; hence the color will be **blue**. Also, Line 17 will make it **all caps**.  

.. code-block:: css
    :linenos: 

    /* asset/css/my_css.css */

    /*class selection*/
    .c_head{
        font-family: cursive;
        color: orange;          /*override the blue color*/
    }


    /*id selection*/
    #i_head{
        color: red;
    }

    /*element selection*/
    p {
        font-variant: small-caps;
        color: blue;
    }


    /*element selection*/
    p {
        color: green;
    }


.. _`fig_css3`:

.. figure:: fig/css3.png

   Priority level for CSS rule


More selectors
==============

:numref:`tbl_list_selectors` shows the combinations of selectors to target the various elements of the HTML. Also, some of the example of 'Attribute selector' is shown in this section. 


.. _`tbl_list_selectors`:

.. table:: List of selectors 

    +--------------------+-----------------------------------------------------------+
    | Selectors          | Description                                               |
    +====================+===========================================================+
    | h1, p, span etc.   | element selector                                          |
    +--------------------+-----------------------------------------------------------+
    | .className         | class selector                                            |
    +--------------------+-----------------------------------------------------------+
    | #idName            | id selector                                               |
    +--------------------+-----------------------------------------------------------+
    | \*                 | Universal selector (selects everything)                   |
    +--------------------+-----------------------------------------------------------+
    | h1.className       | select h1 with class 'className'                          |
    +--------------------+-----------------------------------------------------------+
    | h1#idName          | select h1 with id 'idName'                                |
    +--------------------+-----------------------------------------------------------+
    | p span             | descendant selector (select span which is inside p)       |
    +--------------------+-----------------------------------------------------------+
    | p > span           | child selector ('span' which is direct descendant of 'p') |
    +--------------------+-----------------------------------------------------------+
    | h1, h2, p          | group selection (select h1, h2 and p)                     |
    +--------------------+-----------------------------------------------------------+
    | span[my_id]        | select 'span' with attribute 'my_id'                      |
    +--------------------+-----------------------------------------------------------+
    | span[my_id=m_span] | select 'span' with attribute 'my_id=m_span'               |
    +--------------------+-----------------------------------------------------------+


Attribute selector
------------------


* Add below code at the end of the html file. In these lines 'custom attributes' are added (i.e. my_id).  

.. code-block:: html

    <!-- css.html -->

    <span my_id='m_span'> Span with attribute 'my_id' with value 'm_span' </span>
    <br>
    <span my_id='m_span2'> Span with attribute 'my_id' with value 'm_span2' </span>


* These custom attributes can be selected as below, 


.. code-block:: css

    /*attribute selection*/
    span[my_id] {   /* select 'span' with attribute 'my_id' */
        color: green;
        font-weight: bold
    }


    span[my_id=m_span] { /* select 'span' with attribute 'my_id = m_span' */
        color: red;
    }



More properties
===============

:numref:`tbl_more_properties` shows the some more important properties which can be used in CSS, 

.. _`tbl_more_properties`:

.. table:: More CSS properties

    +----------+------------------------------------+------------------------------------------+
    | Property | Syntax                             | Description/possible values              |
    +==========+====================================+==========================================+
    | size     | 20\%                               | size = 20%                               |
    +----------+------------------------------------+------------------------------------------+
    |          | 20px                               | 20 pixel                                 |
    +----------+------------------------------------+------------------------------------------+
    |          | 2em                                | 2\*font-size                             |
    +----------+------------------------------------+------------------------------------------+
    |          | 2mm, 2cm, 2in                      | 2 mm, cm and inch                        |
    +----------+------------------------------------+------------------------------------------+
    | color    | names                              | e.g. red, blue, green                    |
    +----------+------------------------------------+------------------------------------------+
    |          | hex code (#RRGGBB or #RGB)         | #FFF000 or #FFF                          |
    +----------+------------------------------------+------------------------------------------+
    |          | rgb(num, num, num)                 | rgb(0, 0, 255) or rgb(20%, 10%, 70%)     |
    +----------+------------------------------------+------------------------------------------+
    | link     | a\:link                            | a\:link {color\: red}                    |
    +----------+------------------------------------+------------------------------------------+
    |          | a\:hover                           |                                          |
    +----------+------------------------------------+------------------------------------------+
    |          | a\:visited                         |                                          |
    +----------+------------------------------------+------------------------------------------+
    |          | a\:active                          |                                          |
    +----------+------------------------------------+------------------------------------------+
    | Font     | font-family                        | serif, cursive                           |
    +----------+------------------------------------+------------------------------------------+
    |          | font-style                         | normal, italic, oblique                  |
    +----------+------------------------------------+------------------------------------------+
    |          | font-variant                       | normal, small-caps                       |
    +----------+------------------------------------+------------------------------------------+
    |          | font-weight                        | normal, bold, bolder, lighter, 100-900   |
    +----------+------------------------------------+------------------------------------------+
    |          | font-size                          | 10px, small, medium, large etc.          |
    +----------+------------------------------------+------------------------------------------+
    | Text     | color                              | red, #FFF                                |
    +----------+------------------------------------+------------------------------------------+
    |          | letter-spacing                     | 10px                                     |
    +----------+------------------------------------+------------------------------------------+
    |          | word-spacing                       | 10 px                                    |
    +----------+------------------------------------+------------------------------------------+
    |          | text-align                         | right, left, center                      |
    +----------+------------------------------------+------------------------------------------+
    |          | text-decoration                    | underline, overline, line-through, none  |
    +----------+------------------------------------+------------------------------------------+
    |          | text-transform                     | capitalize, uppercase, lowercase, none   |
    +----------+------------------------------------+------------------------------------------+
    |          | white-space                        | pre, normal, nowrap                      |
    +----------+------------------------------------+------------------------------------------+
    |          | text-shadow                        | text-shadow\:5px 5px 8px red;            |
    +----------+------------------------------------+------------------------------------------+
    | Image    | border                             | '1px', or  '1px solid blue'              |
    +----------+------------------------------------+------------------------------------------+
    |          | height, width                      | 100px, 20%                               |
    +----------+------------------------------------+------------------------------------------+
    | Border   | border-style                       | solid, dashed, dotted, double, none etc. |
    +----------+------------------------------------+------------------------------------------+
    |          | border-top-style                   |                                          |
    +----------+------------------------------------+------------------------------------------+
    |          | border-bottom-style                |                                          |
    +----------+------------------------------------+------------------------------------------+
    |          | border-left-style                  |                                          |
    +----------+------------------------------------+------------------------------------------+
    |          | border-right-style                 |                                          |
    +----------+------------------------------------+------------------------------------------+
    |          | border-width                       | 4px, 4pt                                 |
    +----------+------------------------------------+------------------------------------------+
    |          | border-bottom-width                | similarly use 'top', 'left', 'right'     |
    +----------+------------------------------------+------------------------------------------+
    |          | border (shortcut)                  | 1px solid blue'                          |
    +----------+------------------------------------+------------------------------------------+
    | Margin   | margin, margin-left etc.           |                                          |
    +----------+------------------------------------+------------------------------------------+
    | Padding  | padding (top, bottom, left, right) | '10px 10px 2px 2px' or '10px 2px'        |
    +----------+------------------------------------+------------------------------------------+
    |          | padding-right, padding-left etc.   |                                          |
    +----------+------------------------------------+------------------------------------------+
















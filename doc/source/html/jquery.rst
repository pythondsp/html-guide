.. _`ch_jQuery`:

jQuery
******


Introduction
============

jQuery is the Javascipt library. 


Requirements
------------

* First create a file 'jQuery.html'. Next, we need three files, i.e. one CSS (Line 9) and two Javascipt (Lines 23-24), for using the jQuery in this tutorial. These files are saved inside the folder 'asset', which can be downloaded from the repository. Lines 9 and 24 are the bootstrap files, whereas Line 23 is the actual 'jQuery' file which is required to use the jQuery. 

.. note:: 

    Note that, the 'jQuery.js' files should be placed above the 'bootstrap.js' file, otherwise the bootstrap-javascipt will not work. 

.. code-block:: html
    :linenos:
    :emphasize-lines: 9, 23-24

    <!-- jQuery.html  -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>jQuery</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom styles below -->

    </head>
    <body>

        





        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>

    </body>
    </html>


* If we open this html file in the browser, the a blank page will be displayed with header 'jQuery'. 
  
Add contents
------------

Next add one jumbotron (Line 15-19) and few labels (Lines 21-26) in the html file as shown in :numref:`fig_jquery1`. 


.. code-block:: html
    :linenos:
    :emphasize-lines: 15-19, 21-26

    <!-- jQuery.html  -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>jQuery</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom styles below -->

    </head>
    <body>

        <div class="jumbotron">
            <div class="col-md-offset-4 container">
                <h1><b>jQuery</b></h1>
            </div>  
        </div>

        <span class="label label-lg label-default" id="l_default">Default</span>
        <span class="label label-primary" id="l_primary">Primary</span>
        <span class="label label-success" id="l_success">Success</span>
        <span class="label label-info" id="l_info">Info</span>
        <span class="label label-warning" id="l_warning">Warning</span>
        <span class="label label-danger" id="l_danger">Danger</span>




        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>

    </body>
    </html>


.. _`fig_jquery1`:

.. figure:: fig/jquery1.png

   Jumbotron and labels are added


.. _`sec_jquery_ex`:

jQuery examples
===============

Following are the important parts for writing a jQuery, 

* **Selectors** : to select the specific element (e.g. 'id', 'class' etc.) for operations.
* **Event handling** : perform some operations (hide, toggle etc.) on the selected items at some events (e.g. click, mouser over etc.).

In this section, we will see the working of of jQuery with the help of some examples. Then in :numref:`sec_selectors`, we will see various methods to use the selectors to target the specific elements. Next, a list of operations are shown in :numref:`tbl_operations`. Finally in :numref:`sec_events_handle`, a list of events are shown which can be used to initiate the operations. 


Add jQuery code
---------------

Now, we can add the jQuery code as shown in Lines 36-40. Please note the following part, 

* \$ sign is used to hit the jQuery. In the simple words, we can say that it execute the jQuery.  
* The code is written between the following block. This block stops the jQuery to execute until the whole page is loaded, i.e. jQuery will be executed after completion of the page-download. 
   
    .. code-block:: javascript
    
        $(function() {
                // write code here
        });


* The above block is a shorthand for below, 

    .. code-block:: javascript
    
        $(document).ready(function() {
                // write code here
        });


.. note:: 

    We can use multiple blocks, if we want to separate the codes; or we can write all the jQueries within one block. 


* Lastly, the below line hides the id "l_default" after 1000 ms, i.e. 'Default' label will not be displayed after 1000 ms as shown in :numref:`fig_jquery2`

    .. code-block:: javascript
    
        $("#l_default").hide(1000); // hide id = 'l_default' using # operator after 300 ms


.. code-block:: html
    :linenos:
    :emphasize-lines: 36-40
    
    <!-- jQuery.html  -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>jQuery</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom styles below -->

    </head>
    <body>

        <div class="jumbotron">
            <div class="col-md-offset-4 container">
                <h1><b>jQuery</b></h1>
            </div>  
        </div>

        <span class="label label-lg label-default" id="l_default">Default</span>
        <span class="label label-primary" id="l_primary">Primary</span>
        <span class="label label-success" id="l_success">Success</span>
        <span class="label label-info" id="l_info">Info</span>
        <span class="label label-warning" id="l_warning">Warning</span>
        <span class="label label-danger" id="l_danger">Danger</span>




        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $(function() {
                $("#l_default").hide(1000); // hide id = 'l_default' using # operator after 300 ms
            });
        </script>
    </body>
    </html>


.. _`fig_jquery2`:

.. figure:: fig/jquery2.png

   Label 'Default' is hidden after 1000 ms



.. note::
    
    '\#'' is used to select the 'id'; whereas '\.' is sued to access the 'class'. 



* In the below code, the label 'Danger' is removed; and "opacity='0.5'" is set for rest of the labels. Here, '.label' selects all the classes which have the name 'label' in it. Since all the entries have classes with name 'label', therefore all are selected and faded according to 'opacity='0.5', as shown in :numref:`fig_jquery3`.
  
.. code-block:: html
    :linenos:
    :emphasize-lines: 39-40

    <!-- jQuery.html  -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>jQuery</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom styles below -->

    </head>
    <body>

        <div class="jumbotron">
            <div class="col-md-offset-4 container">
                <h1><b>jQuery</b></h1>
            </div>  
        </div>

        <span class="label label-lg label-default" id="l_default">Default</span>
        <span class="label label-primary" id="l_primary">Primary</span>
        <span class="label label-success" id="l_success">Success</span>
        <span class="label label-info" id="l_info">Info</span>
        <span class="label label-warning" id="l_warning">Warning</span>
        <span class="label label-danger" id="l_danger">Danger</span>




        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $(function() {
                $("#l_default").hide(1000); // hide id = 'l_default' using # operator after 300 ms
                $(".label-danger").hide();  // hide class = 'label' using . operator
                $(".label").css({opacity:'0.5'}); // add opacity to all classes with name 'label'
            });
        </script>
    </body>
    </html>


.. _`fig_jquery3`:

.. figure:: fig/jquery3.png

   Faded labels


jQuery in separate file
-----------------------

It is better to write the jQuery in the separate files, therefore the codes are more manageable. 

* For this, first write the code in the separate file 'my_jquery.js' as below, 

.. note::

    'html' is used to change the content of the 'id' as shown in Line 5 below listing (see :numref:`fig_jquery4`)


.. code-block:: javascript
    :linenos:
    :emphasize-lines: 5

    // asset/js/my_jquery.js

    $(function() {
            $("#l_default").hide(1000); // hide id = 'l_default' using # operator after 300 ms
            $(".label-danger").html("Shark is dangerous");  // change content using 'html'
    });



* Next, import this file to 'html' as shown in Line 35 of below code, 

.. code-block:: html
    :linenos:
    :emphasize-lines: 35

    <!-- jQuery.html  -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>jQuery</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom styles below -->

    </head>
    <body>

        <div class="jumbotron">
            <div class="col-md-offset-4 container">
                <h1><b>jQuery</b></h1>
            </div>  
        </div>

        <span class="label label-lg label-default" id="l_default">Default</span>
        <span class="label label-primary" id="l_primary">Primary</span>
        <span class="label label-success" id="l_success">Success</span>
        <span class="label label-info" id="l_info">Info</span>
        <span class="label label-warning" id="l_warning">Warning</span>
        <span class="label label-danger" id="l_danger">Danger</span>




        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
        <script src="asset/js/my_jquery.js"></script>

    </body>
    </html>


.. _`fig_jquery4`:

.. figure:: fig/jquery4.png

   Changed content using 'html' option


Get input from user
-------------------

Below is the complete html code, which we will use in this section, 

.. code-block:: html
    :linenos:
    :caption: complete HTML code
    :name: html_complete

    <!-- jQuery.html  -->

    <!DOCTYPE html>
    <html>
    <head>
        <title>jQuery</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom styles below -->

    </head>
    <body>

        <div class="jumbotron">
            <div class="col-md-offset-4 container">
                <h1><b>jQuery</b></h1>
            </div>  
        </div>

        <span class="label label-lg label-default" id="l_default">Default</span>
        <span class="label label-primary" id="l_primary">Primary</span>
        <span class="label label-success" id="l_success">Success</span>
        <span class="label label-info" id="l_info">Info</span>
        <span class="label label-warning" id="l_warning">Warning</span>
        <span class="label label-danger" id="l_danger">Danger</span>

        <br><br>
        <span class="btn btn-sm btn-warning" id="b_warning">Toggle Warning Label</span>

        <br><br>
        <!-- label_id is custom attribute whouse values are set according to id of above labels-->
        <span class="btn btn-sm btn-default label-btn" label_id='l_default'>Toggle Default Label</span>
        <span class="btn btn-sm btn-primary label-btn" label_id='l_primary'>Toggle Primary Label</span>
        <span class="btn btn-sm btn-success label-btn" label_id='l_success'>Toggle Success Label</span>
        <span class="btn btn-sm btn-info label-btn" label_id='l_info'>Toggle Info Label</span>
        <span class="btn btn-sm btn-warning label-btn" label_id='l_warning'>Toggle Warning Label</span>
        <span class="btn btn-sm btn-danger label-btn" label_id='l_danger'>Toggle Danger Label</span>
        

        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
        <script src="asset/js/my_jquery.js"></script>

    </body>
    </html>

Toggle label on mouse click
^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this section, the 'mouse click' on the button 'Toggle Warning Label (Line 29)' is read; and then the label 'warning' is toggled. 

* The method 'on' waits for user's input 'click (i.e. mouse click)'.
* On mouse-click next query i.e. 'toggle' is executed which hides/unhide the 'warning label' as shown in :numref:`fig_jquery5` - :numref:`fig_jquery6`.  

.. code-block:: javascript
    :linenos:
    :emphasize-lines: 10-15

    // asset/js/my_jquery.js

    // 'hide label' and 'change content'
    $(function() {
            $("#l_default").hide(1000); // hide id = 'l_default' using # operator after 300 ms
            $(".label-danger").html("Shark is dangerous");  // change content using 'html'
    });


    // toggle 'warning label' based on click on the button 'Toggle Warning Label'
    $(function() {
        $('#b_warning').on('click', function(){
            $('#l_warning').toggle();
        });
    });


.. _`fig_jquery5`:

.. figure:: fig/jquery5.png

   Hide label 'warning'

.. _`fig_jquery6`:

.. figure:: fig/jquery6.png

   Show label 'warning'


Toggle label based on the buttons
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In previous section, we wrote the code which can toggle only one label. In this section, we will toggle multiple labels by writing one-generalized-jQuery as shown in :numref:`fig_jquery7`. 

.. _`fig_jquery7`:

.. figure:: fig/jquery7.png

   Toggle labels based on button-clicked


* Lines 21-26 and 31-38 of :numref:`html_complete` are used for this section,    


* Custom attributes "label_id=..." are added in each button which has the value which corresponds to one of the 'label's id'. e.g. 'label_id' for Line 33 (i.e. l_default) is same as 'id' at Line 21. 

* Next, in the below jQuery, we saved the 'label_id' in a variable (see Line 21) and then the variable is used to toggle the labels. 


.. code-block:: javascript
    :linenos:
    :emphasize-lines: 18-24
    :caption: Toggle the label based on the button-clicked
    :name: jv_code

    // asset/js/my_jquery.js

    // 'hide label' and 'change content'
    $(function() {
            $("#l_default").hide(1000); // hide id = 'l_default' using # operator after 300 ms
            $(".label-danger").html("Shark is dangerous");  // change content using 'html'
    });


    // toggle 'warning label' based on click on the button 'Toggle Warning Label'
    $(function() {
        $('#b_warning').on('click', function(){
            $('#l_warning').toggle();
        });
    });


    // toggle panels according to the clicked-buttons
    $(function(){
        $('.label-btn').on('click', function(){ // select class 'label-btn'
            var labelId = $(this).attr('label_id'); // store the attribute value of 'label_id'
            $('#' + labelId).toggle(); // toggle the label with id  stored in 'label_id'
        });
    });



.. _`sec_selectors`:

Selectors
=========

In this section, we will use following HTML to understand the various combination of the selectors which are summarized in :numref:`tbl_selectors`, 

.. code-block:: html

    <!DOCTYPE html>
    <html>
    <head>
        <title>jQuery Selectors</title>
    </head>
    <body>
        <p>jQuery Selectors</p>

        <p class="foo"> Paragraph with class.</p>
        <p><span>Paragraph with span.</span></p>

        <p id="bar">Paragraph with ID. <span class="foo">Span with class and inside the paragraph-with-id.</span></p>

        <div>
            <p my-id="my_para">Paragraph (with custom id) inside the div</p>
            <p my-id="my_para2"><span>Span inside the paragraph (with custom id) inside the div</span></p>
        </div>


        <!-- Javascript -->
        <script src="asset/js/jquery-3.3.1.min.js"></script>

    </body>
    </html>


.. note:: 

    * Press 'ctrl+shift+k' (or go to Tools->Web Developer->Web Console) to open the web-console in **Firefox** and click on 'console' and type the 'jQuery' commands there. 
    * Press 'ctrl+shift+J' (or go to Tools->Javascript Console) in '**Chrome**' and type the jQuery commands there.

    * In this tutorial, the output of the 'Firefox' are shown. 


Select elements
---------------

Select HTML elements
^^^^^^^^^^^^^^^^^^^^

HTML elements, e.g. div, p, span and table etc., can be selected by writing them with the quotation mark as shown in below codes.


* Here, '\$('p') is the command, whereas the lines below it are the outputs. 
* The outputs show the 'selected elements'; and the total number of selected elements i.e. 'length: 6'. 
* If we put the cursor to one of the outputs, then it will highlight the corresponding elements in the html as shown in :numref:`fig_jquery8`. 

.. code-block:: text

    $('p')
    {…}
    0: <p>
    1: <p class="foo">
    2: <p>
    3: <p id="bar">
    4: <p my-id="my_para">
    5: <p my-id="my_para2">
    length: 6

.. _`fig_jquery8`:

.. figure:: fig/jquery8.png

   Output of \$('p'); (see red-squares)


* Similarly, we can select 'div' elements

.. code-block:: text

    $('div')
    {…}
    0: <div>
    length: 1


Select class
^^^^^^^^^^^^

Class can be selected using '.' operator. In below code, class 'foo' is selected. 

.. code-block:: text

    $(".foo")
    {…}
    0: <p class="foo">
    1: <span class="foo">
    length: 2

Select ID
^^^^^^^^^

ID can be selected using '\#' operator. In the below code, id 'bar' is selected.

.. code-block:: text

    $('#bar')
    […]
    0: <p id="bar">
    length: 1

Combining selectors
^^^^^^^^^^^^^^^^^^^

We can select a element with specific class name using '.' operator as shown in below code. Here element 'span' with class 'foo' is selected, 


.. code-block:: text

    $("span.foo")
    {…}
    0: <span class="foo">
    length: 1


Multiple selectors
^^^^^^^^^^^^^^^^^^

Use ',' to select different types of elements. In the below code, "paragraph with id 'bar'" and "elements with class 'foo'" are selected , 

.. code-block:: text

    $('p#bar, .foo')
    {…}
    0: <p class="foo">
    1: <p id="bar">
    2: <span class="foo">
    length: 3


Select descendant element
^^^^^^^^^^^^^^^^^^^^^^^^^

Select 'span' inside the 'p' (use space between 'span' and 'p'),

.. code-block:: text

    $('p span')
    {…}
    0: <span>
    1: <span class="foo">
    2: <span>
    length: 3


Select child element
^^^^^^^^^^^^^^^^^^^^

The '\>' is used to select the child of an element. 

.. code-block:: text

    $('div>span')
    {…}
    length: 0


.. note:: 

    There is one element inside 'div' but it's path is 'div->p->span' therefore 'span' is a child of 'p' (not of 'div'). But tgis 'span' is the descendant of 'div' therefore can be accessed using space, 

    .. code-block:: text
    
        $('div span')
        {…}
        0: <span>
        length: 1


Attribute selector
^^^^^^^^^^^^^^^^^^

Custom attributes can be selected as below, 

.. code-block:: text

    $('[my-id = my_para]')
    {…}
    0: <p my-id="my_para">
    length: 1

    $('p[my-id != my_para]')  // select p with 'my-id != my_para'
    {…}
    0: <p>
    1: <p class="foo">
    2: <p>
    3: <p id="bar">
    4: <p my-id="my_para2">
    length: 5

    $('p[class = foo]')
    {…}
    0: <p class="foo">
    length: 1



.. _`sec_filter`:

Filters
-------

Various filter options (e.g. filter, first, last and has etc.) can be used after '\:' as shown below, 

.. code-block:: text

    $('p:first')  // select first element
    {…}
    0: p


    $('p:last')  // last element
    {…}
    0: <p my-id="my_para2">
    length: 1

    $('p:not(.foo)')  // 'p' which does not have class 'foo'
    {…}
    0: <p>
    1: <p>
    2: <p id="bar">
    3: <p my-id="my_para">
    4: <p my-id="my_para2">
    length: 5

    $('p:has(span)')  // 'p' which has 'span' inside it
    {…}
    0: <p>
    1: <p id="bar">
    2: <p my-id="my_para2">
    length: 3

    $('p:hidden') // hidden 'p'
    {…}
    length: 0

    $('p:visible') // visible 'p'
    {…}
    0: <p>
    1: <p class="foo">
    2: <p>
    3: <p id="bar">
    4: <p my-id="my_para">
    5: <p my-id="my_para2">
    length: 6

    $('p:contains(with class)') // paragraph which contains class inside it
    {…}
    0: <p class="foo">
    1: <p id="bar">
    length: 2

    $('p').filter(".foo") // same as p.foo
    {…}
    0: <p class="foo">
    length: 1

    $('p').not(".foo")  // p with not class foo
    {…}
    0: <p>
    1: <p>
    2: <p id="bar">
    3: <p my-id="my_para">
    4: <p my-id="my_para2">
    length: 5
 
    $('p').last()  // last element of 'p'
    {…}
    0: <p my-id="my_para2">
    length: 1

    $('p').has('span')  // p with span
    {…}
    0: <p>
    1: <p id="bar">
    2: <p my-id="my_para2">
    length: 3

    $('p').is('.foo')  // does 'p' contain 'foo'
    true

    $('p').children()  // children of 'p'
    {…}
    0: <span>
    1: <span class="foo">
    2: <span>
    length: 3

    $('p').children(".foo") // child of 'p' with class 'foo'
    {…}
    0: <span class="foo">
    length: 1

.. _`tbl_selectors`:

.. table:: Selectors

    +-----------------------------+----------------------------------------------+-------------------------+
    | Command                     | Description                                  | example                 |
    +=============================+==============================================+=========================+
    | \$('elementName')           | select the tags (p, body, table)             | \$('p'), \$('body')     |
    +-----------------------------+----------------------------------------------+-------------------------+
    | \$('.className')            | select element with 'className'              | $(".foo")               |
    +-----------------------------+----------------------------------------------+-------------------------+
    | \$('elementName.className') | element with className                       | $("span.foo")           |
    +-----------------------------+----------------------------------------------+-------------------------+
    | $(selector1, selector2)     | select selector1 and selector2               | $('p#bar, .foo')        |
    +-----------------------------+----------------------------------------------+-------------------------+
    | $(selector1 selector2)      | select selector2 inside selector1            | $('p span')             |
    +-----------------------------+----------------------------------------------+-------------------------+
    | $(selector1>selector2)      | select selector2 which is child of selector1 | $('div>span')           |
    +-----------------------------+----------------------------------------------+-------------------------+
    | $(attributName = someName)  | select attribute with someName               | $('[my-id = my_para]')  |
    +-----------------------------+----------------------------------------------+-------------------------+
    | $p(attributName = someName) | select 'p' with attribute with someName      | $('p[my-id = my_para]') |
    +-----------------------------+----------------------------------------------+-------------------------+
    | See :numref:`sec_filter` for filters                                                                 |
    +-----------------------------+----------------------------------------------+-------------------------+

.. _`sec_operations`:

Operations
==========

In :numref:`jv_code` we saw some operations e.g. 'hide()' and 'toggle()'. :numref:`tbl_operations` shows some more such operations. 

.. _`tbl_operations`:

.. table:: Operations 

    +----------------+-------------+------------------+--------------------+
    | Operations     |             |                  |                    |
    +================+=============+==================+====================+
    | hide()         | hide(speed) | fadeIn(speed)    | fadeOut(speed)     |
    +----------------+-------------+------------------+--------------------+
    | show()         | show(speed) | slideDown(speed) | slideToggle(speed) |
    +----------------+-------------+------------------+--------------------+
    | slideUp(speed) | toggle()    | toggle(speed)    | toggle(switch)     |
    +----------------+-------------+------------------+--------------------+



.. _`sec_events_handle`:

Event handling
==============

In :numref:`jv_code` we saw one example of event handling, where certain operations are performed on the event 'mouse click'. :numref:`tbl_operations` shows a list of some more events. 

.. _`tbl_event_handle`:

.. table:: Event handle

    +-----------+-----------+----------+-------+
    | Events    |           |          |       |
    +===========+===========+==========+=======+
    | click     | keydown   | keypress | keyup |
    +-----------+-----------+----------+-------+
    | mousedown | mouseover |          |       |
    +-----------+-----------+----------+-------+

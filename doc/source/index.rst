.. Bootstrap, Javascript and Jquery documentation master file, created by
   sphinx-quickstart on Sun Jan 28 12:37:51 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

HTML, CSS, Bootstrap, JavaScript and jQuery
===========================================

.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: Contents:

   html/html
   html/css
   html/bootstrap
   html/javascript
   html/jquery

